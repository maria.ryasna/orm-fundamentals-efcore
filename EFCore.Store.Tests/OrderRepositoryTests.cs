﻿using EFCore.Store.Core;
using EFCore.Store.Core.Entities;
using EFCore.Store.Core.Enums;
using EFCore.Store.Core.Interfaces;
using Microsoft.EntityFrameworkCore;
using NUnit.Framework;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace EFCore.Store.Tests
{
    [TestFixture]
    public class OrderRepositoryTests
    {
        private StoreDbContext _db;
        private IUnitOfWork _uow;
        [SetUp]
        public void SetUpOrderRepository()
        {
            var options = new DbContextOptionsBuilder<StoreDbContext>()
                .UseInMemoryDatabase(databaseName: "InMemoryDatabase")
                .Options;
            _db = new StoreDbContext(options);
            _uow = new UnitOfWork(_db);
            _db.Database.EnsureDeleted();
            _db.Products.Add(new Product { Id = 1, Name = "Product 1", Description = "Description 1", Weight = 12, Height = 12, Width = 12, Length = 12 });
            _db.Products.Add(new Product { Id = 2, Name = "Product 2", Description = "Description 2", Weight = 15, Height = 17, Width = 12, Length = 19 });
            _db.Products.Add(new Product { Id = 3, Name = "Product 3", Description = "Description 3", Weight = 9, Height = 9, Width = 9, Length = 9 });
            _db.Products.Add(new Product { Id = 4, Name = "Product 4", Description = "Description 4", Weight = 13, Height = 10, Width = 12, Length = 18 });
            _db.Products.Add(new Product { Id = 5, Name = "Product 5", Description = "Description 5", Weight = 5, Height = 5, Width = 5, Length = 5 });
            _db.Orders.Add(new Order { Id = 1, Status = OrderStatus.InProgress, CreatedDate = DateTime.Parse("25/06/2022 00:00:00"), UpdatedDate = DateTime.Parse("25/01/2022 00:00:00"), ProductId = 1 });
            _db.Orders.Add(new Order { Id = 2, Status = OrderStatus.InProgress, CreatedDate = DateTime.Parse("25/04/2022 00:00:00"), UpdatedDate = DateTime.Parse("30/05/2022 00:00:00"), ProductId = 2 });
            _db.Orders.Add(new Order { Id = 3, Status = OrderStatus.Arrived, CreatedDate = DateTime.Parse("25/04/2022 00:00:00"), UpdatedDate = DateTime.Parse("25/05/2022 00:00:00"), ProductId = 2 });
            _db.Orders.Add(new Order { Id = 4, Status = OrderStatus.Done, CreatedDate = DateTime.Parse("25/03/2022 00:00:00"), UpdatedDate = DateTime.Parse("26/03/2022 00:00:00"), ProductId = 3 });
            _db.Orders.Add(new Order { Id = 5, Status = OrderStatus.Cancelled, CreatedDate = DateTime.Parse("25/03/2022 00:00:00"), UpdatedDate = DateTime.Parse("27/03/2022 00:00:00"), ProductId = 4 });
            _db.SaveChanges();
        }

        [Test]
        public async Task CreateAsync_ShouldCreateNewOrder()
        {
            // Arrange
            Order order = new Order {
                Id = 6,
                Status = OrderStatus.Done, 
                CreatedDate = DateTime.Parse("25/05/2022 00:00:00"), 
                UpdatedDate = DateTime.Parse("25/05/2022 00:00:00"), 
                ProductId = 1
            };

            // Act
            await _uow.OrderRepository.CreateAsync(order);

            // Assert
            Assert.True(await _db.Orders.ContainsAsync(order));
        }

        [Test]
        public async Task DeleteAsync_ShouldRemoveOrder()
        {
            // Arrange
            var expected = 4;

            // Act
            await _uow.OrderRepository.DeleteAsync(1);
            var actual = (await _db.Orders.ToListAsync()).Count;

            // Assert
            Assert.AreEqual(expected, actual);
        }
        [Test]
        public async Task ReadAsync_ShouldReturnOrder()
        {
            // Arrange
            var expected = new Order { 
                Id = 1, 
                Status = OrderStatus.InProgress, 
                CreatedDate = DateTime.Parse("25/06/2022 00:00:00"), 
                UpdatedDate = DateTime.Parse("25/01/2022 00:00:00"), 
                ProductId = 1 
            };

            // Act
            var actual = await _uow.OrderRepository.ReadAsync(1);

            // Assert
            Assert.AreEqual(expected.ProductId, actual.ProductId);
            Assert.AreEqual(expected.CreatedDate.ToShortDateString(), actual.CreatedDate.ToShortDateString());
            Assert.AreEqual(expected.UpdatedDate.ToShortDateString(), actual.UpdatedDate.ToShortDateString());
            Assert.AreEqual(expected.Status, actual.Status);
        }
        [Test]
        public async Task UpdateAsync_ShouldUpdateOrder()
        {
            // Arrange
            Order order = new Order { 
                Id = 3, 
                Status = OrderStatus.Done,  
                ProductId = 2 
            };
            var expected = DateTime.Now;

            // Act
            await _uow.OrderRepository.UpdateAsync(order);
            var actual = await _db.Orders.Where(o => o.Id == order.Id).FirstOrDefaultAsync();

            // Assert
            Assert.AreEqual(expected.ToShortDateString(), actual.UpdatedDate.ToShortDateString());
        }

        [Test]
        public async Task GetAllOrdersByFilterAsync_ShouldReturnAllOrdersWhichMatchFilter()
        {
            // Arrange
            var expected = 2;
            var month = 5;
            var productId = 2;

            // Act
            var actual = (await _uow.OrderRepository.GetAllOrdersByFilterAsync(month: month, productId: productId)).ToList().Count;

            // Assert
            Assert.AreEqual(expected, actual);
        }
    }
}
