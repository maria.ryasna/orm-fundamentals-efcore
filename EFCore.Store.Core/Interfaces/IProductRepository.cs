﻿using EFCore.Store.Core.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EFCore.Store.Core.Interfaces
{
    public interface IProductRepository : IRepository<Product>
    {
        Task<IEnumerable<Product>> GetAllProductsAsync();
    }
}
