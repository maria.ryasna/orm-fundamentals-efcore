﻿using EFCore.Store.Core.Entities;
using Microsoft.EntityFrameworkCore;
using System;

namespace EFCore.Store.Core
{
    public class StoreDbContext : DbContext
    {
        private readonly string _connectionString;
        public DbSet<Order> Orders { get; set; }
        public DbSet<Product> Products { get; set; }
        public StoreDbContext() : base() { }
        public StoreDbContext(DbContextOptions<StoreDbContext> options) : base(options) { }
        public StoreDbContext(string connectionString) : base()
        {
            _connectionString = connectionString ?? throw new ArgumentNullException(nameof(connectionString));
        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer(_connectionString);
            }
        }
    }
}
