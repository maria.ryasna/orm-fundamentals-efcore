﻿using System.ComponentModel.DataAnnotations;

namespace EFCore.Store.Core.Entities
{
    public abstract class BaseEntity
    {
        [Key]
        public int Id { get; set; }
    }
}
