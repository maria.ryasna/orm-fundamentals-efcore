using EFCore.Store.Core;
using EFCore.Store.Core.Entities;
using EFCore.Store.Core.Interfaces;
using Microsoft.EntityFrameworkCore;
using NUnit.Framework;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EFCore.Store.Tests
{
    public class Tests
    {
        private StoreDbContext _db;
        private IUnitOfWork _uow;
        [SetUp]
        public void SetUpProductRepository()
        {
            var options = new DbContextOptionsBuilder<StoreDbContext>()
                .UseInMemoryDatabase(databaseName: "InMemoryDatabase")
                .Options;
            _db = new StoreDbContext(options);
            _uow = new UnitOfWork(_db);
            _db.Database.EnsureDeleted();
            _db.Products.Add(new Product { Id = 1, Name = "Product 1", Description = "Description 1", Weight = 12, Height = 12, Width = 12, Length = 12 });
            _db.Products.Add(new Product { Id = 2, Name = "Product 2", Description = "Description 2", Weight = 15, Height = 17, Width = 12, Length = 19 });
            _db.Products.Add(new Product { Id = 3, Name = "Product 3", Description = "Description 3", Weight = 9, Height = 9, Width = 9, Length = 9 });
            _db.Products.Add(new Product { Id = 4, Name = "Product 4", Description = "Description 4", Weight = 13, Height = 10, Width = 12, Length = 18 });
            _db.Products.Add(new Product { Id = 5, Name = "Product 5", Description = "Description 5", Weight = 5, Height = 5, Width = 5, Length = 5 });
            _db.SaveChanges();
        }

        [Test]
        public async Task CreateAsync_ShouldCreateNewProduct()
        {
            // Arrange
            Product product = new Product
            {
                Id = 6,
                Name = "Product 6",
                Description = "Description 6",
                Weight = 34,
                Height = 234,
                Width = 654,
                Length = 123
            };

            // Act
            await _uow.ProductRepository.CreateAsync(product);

            // Assert
            Assert.True(await _db.Products.ContainsAsync(product));
        }

        [Test]
        public async Task DeleteAsync_ShouldRemoveProduct()
        {
            // Arrange
            var expected = 4;

            // Act
            await _uow.ProductRepository.DeleteAsync(1);
            var actual = (await _db.Products.ToListAsync()).Count;

            // Assert
            Assert.AreEqual(expected, actual);
        }
        [Test]
        public async Task ReadAsync_ShouldReturnProduct()
        {
            // Arrange
            var expected = new Product {
                Id = 2,
                Name = "Product 2",
                Description = "Description 2",
                Weight = 15,
                Height = 17,
                Width = 12,
                Length = 19
            };

            // Act
            var actual = await _uow.ProductRepository.ReadAsync(2);

            // Assert
            Assert.AreEqual(expected.Name, actual.Name);
            Assert.AreEqual(expected.Description, actual.Description);
            Assert.AreEqual(expected.Weight, actual.Weight);
            Assert.AreEqual(expected.Height, actual.Height);
            Assert.AreEqual(expected.Width, actual.Width);
            Assert.AreEqual(expected.Length, actual.Length);
        }
        [Test]
        public async Task UpdateAsync_ShouldUpdateOrder()
        {
            // Arrange
            var expected = new Product
            {
                Id = 2,
                Name = "New name",
                Description = "New description",
                Weight = 25,
                Height = 27,
                Width = 22,
                Length = 29
            };

            // Act
            await _uow.ProductRepository.UpdateAsync(expected);
            var actual = await _db.Products.Where(o => o.Id == expected.Id).FirstOrDefaultAsync();

            // Assert
            Assert.AreEqual(expected.Name, actual.Name);
            Assert.AreEqual(expected.Description, actual.Description);
            Assert.AreEqual(expected.Weight, actual.Weight);
            Assert.AreEqual(expected.Height, actual.Height);
            Assert.AreEqual(expected.Width, actual.Width);
            Assert.AreEqual(expected.Length, actual.Length);
        }

        [Test]
        public async Task GetAllProductsReturnProductList()
        {
            // Arrange
            var expected = 5;

            // Arrange & Act
            var actual = (await _uow.ProductRepository.GetAllProductsAsync()).ToList().Count;

            // Assert
            Assert.AreEqual(expected, actual);
        }
    }
}