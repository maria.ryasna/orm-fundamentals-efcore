﻿using EFCore.Store.Core.Entities;
using EFCore.Store.Core.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EFCore.Store.Core.Repositories
{
    public class ProductRepository : IProductRepository
    {
        private readonly StoreDbContext _db;
        public ProductRepository(StoreDbContext context)
        {
            _db = context ?? throw new ArgumentNullException(nameof(context));
        }
        public async Task CreateAsync(Product product)
        {
            if (product is null)
            {
                throw new ArgumentNullException(nameof(product));
            }
            _db.Products.Add(product);
            await _db.SaveChangesAsync();
        }

        public async Task DeleteAsync(int id)
        {
            _db.Products.Remove(_db.Products.Find(id));
            await _db.SaveChangesAsync();
        }

        public async Task<IEnumerable<Product>> GetAllProductsAsync()
        {
            return await Task.Run(() => _db.Products);
        }

        public async Task<Product> ReadAsync(int id)
        {
            return await _db.Products.FindAsync(id);
        }

        public async Task UpdateAsync(Product product)
        {
            if (product is null)
            {
                throw new ArgumentNullException(nameof(product));
            }
            var updatedProduct = await _db.Products.Where(o => o.Id == product.Id).FirstOrDefaultAsync();
            updatedProduct.Name = product.Name;
            updatedProduct.Description = product.Description;
            updatedProduct.Weight = product.Weight;
            updatedProduct.Height = product.Height;
            updatedProduct.Width = product.Width;
            updatedProduct.Length = product.Length;
            _db.Products.Update(updatedProduct);
            await _db.SaveChangesAsync();
        }
    }
}
