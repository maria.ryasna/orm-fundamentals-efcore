﻿using EFCore.Store.Core.Interfaces;
using EFCore.Store.Core.Repositories;
using System.Threading.Tasks;

namespace EFCore.Store.Core
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly StoreDbContext _db;
        private IOrderRepository _orderRepository { get; }
        private IProductRepository _productRepository { get; }
        public UnitOfWork(StoreDbContext db)
        {
            _db = db;
            _orderRepository = new OrderRepository(db);
            _productRepository = new ProductRepository(db);
        }
        public IOrderRepository OrderRepository { get { return _orderRepository; } }
        public IProductRepository ProductRepository { get { return _productRepository; } }
        public async Task<int> SaveAsync()
        {
            return await _db.SaveChangesAsync();
        }

    }
}
