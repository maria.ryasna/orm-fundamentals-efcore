﻿using EFCore.Store.Core.Entities;
using EFCore.Store.Core.Enums;
using EFCore.Store.Core.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace EFCore.Store.Core.Repositories
{
    public class OrderRepository : IOrderRepository
    {
        private readonly StoreDbContext _db;
        public OrderRepository(StoreDbContext context)
        {
            _db = context ?? throw new ArgumentNullException(nameof(context));
        }

        public async Task BulkDeleteOrdersAsync(int? month = null, OrderStatus? status = null, int? year = null, int? productId = null)
        {
            using var transaction = _db.Database.BeginTransaction();
            try
            {
                await _db.Orders
                .Where((order) => !(!(order.CreatedDate.Month == month) && !(order.UpdatedDate.Month == month)
                    && !(order.CreatedDate.Year == year) && !(order.UpdatedDate.Year == year)
                    && !(order.Status == status) && !(order.ProductId == productId))
                )
                .ForEachAsync((order) => {
                    _db.Orders.Remove(order);
                    _db.SaveChanges();
                    transaction.Commit();
                });
                await _db.SaveChangesAsync();
            }
            catch (Exception)
            {
                transaction.Rollback();
            }
        }

        public async Task CreateAsync(Order order)
        {
            if (order is null)
            {
                throw new ArgumentNullException(nameof(order));
            }
            _db.Orders.Add(order);
            await _db.SaveChangesAsync();
        }

        public async Task DeleteAsync(int id)
        {
            _db.Orders.Remove(_db.Orders.Find(id));
            await _db.SaveChangesAsync();
        }

        public async Task<IEnumerable<Order>> GetAllOrdersByFilterAsync(int? month = null, OrderStatus? status = null, int? year = null, int? productId = null)
        {
            var result = await Task.Run(() => _db.Orders
                .Where((order) => 
                    ((month == null ? true : order.CreatedDate.Month == month) || (month == null ? true : order.UpdatedDate.Month == month))
                    && ((year == null ? true : order.CreatedDate.Year == year) || (year == null ? true : order.UpdatedDate.Year == year))
                    && (status == null ? true : order.Status == status) 
                    && (productId == null ? true : order.ProductId == productId)
                )
                .AsEnumerable());
            return result;
        }

        public async Task<Order> ReadAsync(int id)
        {
            return await _db.Orders
                .Include(order => order.Product)
                .FirstOrDefaultAsync(i => i.Id == id);
        }

        public async Task UpdateAsync(Order order)
        {
            if (order is null)
            {
                throw new ArgumentNullException(nameof(order));
            }
            var updatedOrder = await _db.Orders.Where(o => o.Id == order.Id).FirstOrDefaultAsync();
            updatedOrder.UpdatedDate = DateTime.Now;
            updatedOrder.Status = order.Status;
            updatedOrder.ProductId = order.ProductId;
            _db.Orders.Update(updatedOrder);
            await _db.SaveChangesAsync();
        }
    }
}
