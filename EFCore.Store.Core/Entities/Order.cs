﻿using EFCore.Store.Core.Enums;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace EFCore.Store.Core.Entities
{
    [Table("Order")]
    public class Order : BaseEntity
    {
        public OrderStatus Status { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
        public int ProductId { get; set; }
        public virtual Product Product { get; set; }
    }
}
