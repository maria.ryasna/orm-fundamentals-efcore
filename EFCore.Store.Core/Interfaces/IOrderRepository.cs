﻿using EFCore.Store.Core.Entities;
using EFCore.Store.Core.Enums;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace EFCore.Store.Core.Interfaces
{
    public interface IOrderRepository : IRepository<Order>
    {
        Task<IEnumerable<Order>> GetAllOrdersByFilterAsync(int? month = null, OrderStatus? status = null, int? year = null, int? productId = null);
        Task BulkDeleteOrdersAsync(int? month = null, OrderStatus? status = null, int? year = null, int? productId = null);
    }
}
